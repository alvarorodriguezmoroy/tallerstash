# TallerStash

Proyecto para ofrecer un taller de como utilizar el comando stash de git


Guión del taller:


- Bajarse el Proyecto
- Hacer un git pull
- Crearse una rama y situarse en ella (git checkout -b "nombreRama")
- Git status
- Git add .
- Git commit -m 'creacion de mi rama'
- Git push
- Añadir un fichero a tu proyecto en tu rama
- Hacer un git status para comprobar que el fichero que se acaba de crear esta como untracked (no se ha añadido ni subido, es nuevo)
- Hacer un "git add ." "git commit -m 'creacion de mi rama'" y "git push"
